import pulumi
import pulumi_aws as aws

# Load configuration
config = pulumi.Config()

# Retrieve already existent S3 bucket
bucket = aws.s3.get_bucket(
    bucket=config.require('bucket_name'),
)

# Retrieve already existent object in bucket
s3_object_read = aws.s3.get_bucket_object(
    bucket=bucket.id,
    key=config.require('s3_object_name'),
)

# Display object content (content-type muste be text/* to be readable from pulumi)
pulumi.info(f"Key value : {s3_object_read.body}")

# Output from pulumi up command
#
# Previewing update (plusgrade_assignment)
# 
# View in Browser (Ctrl+O): https://app.pulumi.com/Marc3001/plusgrade_assignment/plusgrade_assignment/previews/627da6b7-99cd-4d89-bc00-f000ab21cde4
# 
#      Type                 Name                                       Plan     Info
#      pulumi:pulumi:Stack  plusgrade_assignment-plusgrade_assignment           2 warnings; 1 message

# Diagnostics:
#   pulumi:pulumi:Stack (plusgrade_assignment-plusgrade_assignment):
#     warning: aws:s3/getBucketObject:getBucketObject verification warning: Use the aws_s3_object data source instead
#     warning: aws:s3/getBucketObject:getBucketObject verification warning: use the aws_s3_object data source instead
#     Key value : My_secret_value

# Resources:
#     1 unchanged

# info: There are no resources in your stack (other than the stack resource).

# Do you want to perform this update? yes
# Updating (plusgrade_assignment)

# View in Browser (Ctrl+O): https://app.pulumi.com/Marc3001/plusgrade_assignment/plusgrade_assignment/updates/35

#      Type                 Name                                       Status     Info
#      pulumi:pulumi:Stack  plusgrade_assignment-plusgrade_assignment             2 warnings; 1 message

# Diagnostics:
#   pulumi:pulumi:Stack (plusgrade_assignment-plusgrade_assignment):
#     warning: aws:s3/getBucketObject:getBucketObject verification warning: Use the aws_s3_object data source instead
#     warning: aws:s3/getBucketObject:getBucketObject verification warning: use the aws_s3_object data source instead
#     Key value : My_secret_value

# Resources:
#     1 unchanged

# Duration: 4s

